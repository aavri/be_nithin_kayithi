# Bank Branches API

A simple REST API that provides bank branch locations.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

JDK 1.8
Apache Maven installed


### Running the Application

java -jar ./target/bank-branches-api-0.0.1-SNAPSHOT.jar


### API

http://localhost:8080/branches - to get list of all branches
http://localhost:8080/branches/{city} - to get list of all branches by city


## Authors

Nithin Kayithi