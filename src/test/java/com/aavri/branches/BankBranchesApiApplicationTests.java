package com.aavri.branches;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.aavri.branches.controller.BranchController;
import com.aavri.branches.domain.Branch;
import com.aavri.branches.service.BranchService;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class BankBranchesApiApplicationTests {
	
	@Autowired
	private MockMvc mockMvc;
	
	@Autowired
	private BranchController controller;
	
	@Autowired
	@InjectMocks
	private BranchService service;
	
	@Test
	public void noParamBranches() throws Exception {

		this.mockMvc.perform(get("/branches")).andDo(print()).andExpect(status().isOk())
				.andExpect(jsonPath("$").isArray());
	}
//
//	@Test
//	public void paramWithBranches() throws Exception {
//
//		this.mockMvc.perform(get("/branches").param("cityname", "SHEFFIELD")).andDo(print()).andExpect(status().isOk())
//				.andExpect(jsonPath("$[0].city").value("SHEFFIELD"));
//	}
	
	@Test
    public void contexLoads() throws Exception {
        assertThat(controller).isNotNull();
    }
	
	@Test
	public void testAllBranches() throws Exception {
		List<Branch> branches = service.getBranches("");
		assertTrue(branches.size() > 0);
	}
	
	@Test(expected= NullPointerException.class)
	public void testAllBranchesNull() throws Exception {
		assertNotNull(service.getBranches(null));
	}

	@Test
	public void testBranchesByCity() throws Exception {
		List<Branch> branches = service.getBranches("SHEFFIELD");
		assertEquals("SHEFFIELD", branches.get(0).getCity());	
		assertNotNull(service.getBranches("sheffield"));
	}

}
