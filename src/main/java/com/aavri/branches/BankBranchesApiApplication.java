package com.aavri.branches;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BankBranchesApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(BankBranchesApiApplication.class, args);
	}

}
