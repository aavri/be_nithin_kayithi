package com.aavri.branches.service;

import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

@Repository
public class BranchesDAO {
	final String API_URL = "https://api.halifax.co.uk/open-banking/v2.2/branches";

	public String getBranchesResponse() {

		String response = new RestTemplate().getForObject(API_URL, String.class);
		return response;

	}
}
