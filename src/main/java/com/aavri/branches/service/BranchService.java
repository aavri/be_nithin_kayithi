package com.aavri.branches.service;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aavri.branches.service.BranchesDAO;
import com.aavri.branches.domain.Branch;

@Service
public class BranchService {
	
	@Autowired
	BranchesDAO dao;
	
	/**
	 * The method fetches the response from the Halifax API and parses the JSON.
	 * The data is then transformed and returned as array of all branches or 
	 * the data is filtered by city name when branches are requested for a specific city.
	 * 
	 * @param city
	 * 
	 */
	public List<Branch> getBranches(String city) {
		
		String branchesResponse = dao.getBranchesResponse();
		List<Branch> branches = new ArrayList<>();
		
		JSONObject jsonObject = new JSONObject(branchesResponse);
		JSONObject data = (JSONObject) jsonObject.getJSONArray("data").get(0);
		JSONObject brand = (JSONObject) data.getJSONArray("Brand").get(0);
		JSONArray branchArray = brand.getJSONArray("Branch");
		
		for (int i = 0; i < branchArray.length(); i++) {
			JSONObject jsonBranch = (JSONObject) branchArray.get(i);
			Branch branch = getBranchDataFromJSON(jsonBranch);
			// Branches filtered by city name
			if (city.length() > 0 && !city.equalsIgnoreCase(branch.getCity())) {
				continue;
			}
			branches.add(branch);
		}
		return branches;
		
	}
	
	/**
	 * Parses JSON object to @Branch class.
	 * 
	 * @param branchJSON
	 * 
	 */
	private Branch getBranchDataFromJSON(JSONObject branchJSON) {
		
		Branch branch = new Branch();
		String branchName = branchJSON.get("Name").toString();
		branch.setBranchName(branchName);
		
		JSONObject postalAddress = (JSONObject) branchJSON.get("PostalAddress");
		String addressLine = (String) postalAddress.getJSONArray("AddressLine").get(0);
		branch.setStreetAddress(addressLine);
		if (!postalAddress.isNull("TownName")) {
			String townName = postalAddress.get("TownName").toString();
			branch.setCity(townName);
		}
		
		if (!postalAddress.isNull("CountrySubDivision")) {
			JSONArray countrySubDivision = postalAddress.getJSONArray("CountrySubDivision");
			String subDiv = (String) countrySubDivision.get(0);
			branch.setCountrySubDivision(subDiv);
		}
		
		branch.setCountry(postalAddress.get("Country").toString());
		branch.setPostCode(postalAddress.get("PostCode").toString());
		JSONObject geoLocation = (JSONObject) postalAddress.get("GeoLocation");
		JSONObject geographicCoordinates = (JSONObject) geoLocation.get("GeographicCoordinates");
		branch.setLongitude(Float.parseFloat(geographicCoordinates.getString("Longitude")));
		branch.setLatitude(Float.parseFloat(geographicCoordinates.getString("Latitude")));
		return branch;
		
	}
}
