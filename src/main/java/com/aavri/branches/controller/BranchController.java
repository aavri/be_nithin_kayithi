package com.aavri.branches.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.aavri.branches.domain.Branch;
import com.aavri.branches.service.BranchService;

@RestController
public class BranchController {
	
	@Autowired
	BranchService branchService;
	
	/**
	 * API Request Mapping to get all branch location for all branches.
	 * REST endpoint: "localhost:port/branches"
	 */
	@RequestMapping("/branches")
	public List<Branch> getAllBranches() {
		return branchService.getBranches("");
	}
	
	/**
	 * API Request Mapping to get all branch location for a specific city.
	 * REST endpoint: "localhost:port/branches/{city}"
	 * @param city
	 * 
	 */
	@RequestMapping("/branches/{city}")
	public List<Branch> getAllBranchesByCity(@PathVariable("city") String city) {
		return branchService.getBranches(city);
	}
}
